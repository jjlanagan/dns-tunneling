import re
import base64

file = open("querylog.txt", "r")

lines = []

for lines in file:
	lines.append(line)

final_string = ''

for query in lines:
	regex_result = re.search(r'(?<=query: )(.*)(?=.malicious)', query)
	if regex_result:
		regex_result = regex_result.group()
		final_string = final_string + regex_result

#print(final_string)

base64_decoded = base64.b64decode(final_string)
utf8_decoded = base64_decoded.decode('utf8')

#print(utf8_decoded)

output_file = open('data.txt', 'w')
output_file.write(utf8_decoded)
output_file.close()
file.close()

print("\nOutput file data.txt created.\n")
